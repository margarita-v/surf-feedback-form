(function() {
    var state = {
        files: {}
    };

    var alertMessages = [
        'Спасибо, скоро мы вам ответим',
        'Вы забыли представиться ;)',
        'Оставьте почту или телефон',
        'Введите, корректный адрес почты',
        'Опишите, пожалуйста, задачу',
    ];

    var alertWrap = null;
    var emailField = document.querySelector('.form [name=email]');

    function renderFileBlocks(files) {
        var attachFiles = document.querySelector('.attach__files');
        attachFiles.innerHTML = null;
        Object.keys(files).forEach(function(key) {
            var block = renderFileBlock(files[key], key);
            attachFiles.appendChild(block);
        })
    }

    function renderFileBlock(file, id) {
        var block = document.createElement("div");
        block.setAttribute('data-id', id);
        block.setAttribute('class', 'attach__item');
        var text = document.createElement("span");
        text.setAttribute('class', 'attach__item-text');
        var button = document.createElement("button");
        button.setAttribute('type', 'button');
        button.setAttribute('data-id', id);
        button.setAttribute('class', 'attach__item-button');
        text.innerHTML = file.name;
        block.appendChild(text);
        block.appendChild(button);
        return block;
    };

    // add file to list
    function attachHandler(e, data) {
        var files = e.target.files;
        Object.keys(files).forEach(function(key) {
            var id = new Date().getTime() + Math.floor(Math.random() * (1000 - 1) + 1);
            state.files[id] = files[key];
        });
        renderFileBlocks(state.files);
    };

    // remove file from list
    function attachCloseHandler(e) {
        if(e.target.classList.contains('attach__item-button')) {
            var fileBlock = e.target;
            var fileBlockId = fileBlock.getAttribute('data-id');
            delete state.files[fileBlockId];
            renderFileBlocks(state.files);
        }
    }

    // remove alert block
    function alertCloseHandler(e) {
        if(e.target.classList.contains('alert__button')) {
            alertWrap.innerHTML = null;
        }
    }

    // validation
    function validate(name) {
        var block = document.querySelector('.form [name='+ name +']');
        return !!block.value;
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    // show Alert block
    function renderAlert(alertMessage, type) {
        var type = type || 'error';
        var block = document.createElement("div");
        block.setAttribute('class', 'alert alert_theme_' + type);
        var text = document.createElement("span");
        text.setAttribute('class', 'alert__text');
        var button = document.createElement("button");
        button.setAttribute('type', 'button');
        button.setAttribute('class', 'alert__button');
        text.innerHTML = alertMessage;
        block.appendChild(text);
        block.appendChild(button);
        alertWrap.innerHTML = null;
        alertWrap.appendChild(block);
    }

    function clearFields() {
        var fields = document.querySelectorAll('.form [name]');
        document.querySelectorAll('.mdl-textfield').forEach(function(field) {
            field.classList.remove('is-focused');
            field.classList.remove('is-dirty');
        });
        fields.forEach(function(field) {
            field.value = null;
        });
        state.files = {};
        renderFileBlocks(state.files);

    }

    function getFormData() {
        var formData = new FormData();
        var fields = document.querySelectorAll('.form [name]');
        fields.forEach(function(field) {
            if ( field.getAttribute('type') === 'file') {
                Object.keys(state.files).forEach(function(key) {
                    var file = state.files[key];
                    formData.append('files[]', file, file.name);
                });
            } else {
                formData.append(field.getAttribute('name'), field.value);
            }
        });
        return formData;
    }

    // submit form
    function formHandler(e) {
        e.preventDefault();
        var alertMessage = null;
        if(!validate('name')) {
            return renderAlert(alertMessages[1]);
        }

        if(!validate('email') && !validate('phone')) {
            return renderAlert(alertMessages[2]);
        }
        if(validate('email') && !validateEmail(emailField.value)) {
            return renderAlert(alertMessages[3]);
        }
        if(!validate('task')) {
            return renderAlert(alertMessages[4]);
        }
        alertWrap.innerHTML = null;

        var formData = getFormData();

        // for (var value of formData.values()) {
        //   console.log(value);
        // }


        // ajax query

        renderAlert(alertMessages[0], 'success');
        clearFields();

    }

    alertWrap = document.querySelector('.panel__alert-wrap');
    var attachInput = document.querySelector('.attach__input');
    var attach = document.querySelector('.attach');
    var form = document.querySelector('.form');

    attachInput.addEventListener('change', attachHandler);
    alertWrap.addEventListener('click', alertCloseHandler);
    attach.addEventListener('click', attachCloseHandler);
    form.addEventListener('submit', formHandler);

    new IMask(document.getElementById('phone'), {
        mask: '+{7}(000)000-00-00'
    });

}());